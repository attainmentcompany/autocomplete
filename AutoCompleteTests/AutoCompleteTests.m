//
//  AutoCompleteTests.m
//  AutoCompleteTests
//
//  Created by Seth Hartwick on 5/5/15.
//  Copyright (c) 2015 Attainment Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AttainmentAutoCompleteDictionary.h"

@interface AutoCompleteTests : XCTestCase

@end

@interface AttainmentAutoCompleteDictionary (ExposePrivate)
- (void)immediatelyAddToDictionary:(NSString *)phraseToAdd;
@end

@implementation AutoCompleteTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAnyPredictionsReturned {
    AttainmentAutoCompleteDictionary *d = [AttainmentAutoCompleteDictionary new];
    
    NSArray *predictions = [d predictionsForPhrase:@"this is" limit:5];
    
    // This is an example of a functional test case.
    XCTAssert(predictions.count > 0, @"Got Predictions");
}


- (void)testGoodSimplePrediction {
    AttainmentAutoCompleteDictionary *d = [AttainmentAutoCompleteDictionary new];
    
    NSArray *predictions = [d predictionsForPhrase:@"this" limit:5];
    
    NSString *firstPrediction = predictions.firstObject;
    
    XCTAssert(firstPrediction != nil, @"Got a prediction");
    
    // This is an example of a functional test case.
    XCTAssert([firstPrediction.lowercaseString isEqualToString:@"this is"], @"Pass");
}


- (void)testGoodLongPrediction {
    AttainmentAutoCompleteDictionary *d = [AttainmentAutoCompleteDictionary new];
    
    NSArray *predictions = [d predictionsForPhrase:@"This eBook is for the use" limit:5];
    
    NSString *firstPrediction = predictions.firstObject;
    
    XCTAssert(firstPrediction != nil, @"Got a prediction");
    
    // This is an example of a functional test case.
    XCTAssert([firstPrediction.lowercaseString isEqualToString:@"This eBook is for the use of"], @"Pass");
}


- (void)testAddingPhrases {
    AttainmentAutoCompleteDictionary *d = [AttainmentAutoCompleteDictionary new];
    d.preventSaving = YES;
    
    NSArray *predictions = [d predictionsForPhrase:@"bleep bloop" limit:5];
    
    XCTAssert(predictions.count == 0, @"Initially no predictions for gibberish");
    
    [d immediatelyAddToDictionary:@"bleep bloop blip"];
    
    predictions = [d predictionsForPhrase:@"bleep bloop" limit:5];
    
    XCTAssert([predictions containsObject:@"bleep bloop blip"], @"Found gibberish suggestion after adding");
}

// TODO: test line limiting

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
