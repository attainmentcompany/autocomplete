//
//  ViewController.h
//  AutoComplete
//
//  Created by Seth Hartwick on 5/5/15.
//  Copyright (c) 2015 Attainment Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *searchTextField;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

