//
//  AppDelegate.m
//  AutoComplete
//
//  Created by Seth Hartwick on 5/5/15.
//  Copyright (c) 2015 Attainment Company. All rights reserved.
//

#import "AppDelegate.h"
//#import "PJTernarySearchTree.h"
#import "AttainmentAutoCompleteDictionary.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    NSArray* words = [d getSimilarWords:@"ly" limit:5];
//    NSLog(@"Words found");
//    NSArray* phrases = [d getMatchingGrams:@"about" limit:5];
//    NSLog(@"Phrases found");
//    
//    NSLog(@"%@", words);
//    NSLog(@"%@", phrases);
//    NSLog(@"Searching phrase 2");
//    phrases = [d getMatchingGrams:@"the" limit:5];
//    NSLog(@"Phrase 2 found");
//    NSLog(@"%@", phrases);
    
    
    
    
    
//    
//    PJTernarySearchTree * tree = [[PJTernarySearchTree alloc]init];
//    NSArray * retrieved = nil;
//    [tree insertString:@"banana"];
//    [tree insertString:@"apps"];
//    [tree insertString:@"apple"];
//    [tree insertString:@"banana republic"];
   // [tree insertString:[d content]];
//    NSString* longString = [d sanitizeString:[d content]];
//    NSArray* singleWords = [longString componentsSeparatedByString:@" "];
//
//    for( id str in singleWords)
//    {
//        BOOL found = false;
//        for( NSString *strFound in [tree retrievePrefix:str])
//        {
//            if( [strFound isEqualToString: str])
//            {
//                found = true;
//            }
//        }
//        if( !found) {
//            [tree insertString:str];
//        }
//    }
    
//        NSMutableArray* ma = [d getNgrams:1];
//        NSArray* wa =  [d getWeightedArray:ma];
//    
//        for( id str in wa)
//        {
//            [tree insertString:str];
//        }
//    
////    NSMutableArray* ma = [d getNgrams:2];
////    NSArray* wa =  [d getWeightedArray:ma];
//    
//    ma = [d getNgrams:2];
//    wa =  [d getWeightedArray:ma];
//    
//    for( id str in wa)
//    {
//        [tree insertString:str];
//    }
//     ma = [d getNgrams:3];
//    wa =  [d getWeightedArray:ma];
//    
//    for( id str in wa)
//    {
//        [tree insertString:str];
//    }
//     ma = [d getNgrams:4];
//     wa =  [d getWeightedArray:ma];
//    
//    for( id str in wa)
//    {
//        [tree insertString:str];
//    }
//     ma = [d getNgrams:5];
//   wa =  [d getWeightedArray:ma];
//    
//    for( id str in wa)
//    {
//        [tree insertString:str];
//    }
//    NSLog(@"Start weighting");
//    NSArray* wa = [d getWeightedArray:[d combineGrams]];
//    for( id str in wa)
//    {
//        [tree insertString:str];
//    }
//    NSLog(@"Start lookup");
//    retrieved = [tree retrievePrefix:@"ban" countLimit:5];
//    NSLog(@"%@", retrieved);
    
    
//    NSString * savePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"test.tree"];
//    
//    
//    PJTernarySearchTree * tree = [PJTernarySearchTree treeWithFile:savePath];
//    
//    if(tree)
//    {
//        NSLog(@"Loaded from file!");
//    }
//    else
//    {
//        NSLog(@"Make a new one, and save it");
//        
//        tree = [[PJTernarySearchTree alloc] init];
//        
//        [tree insertString:@"http://www.peakji.com"];
//        [tree insertString:@"http://www.peak-labs.com"];
//        [tree insertString:@"http://www.facebook.com"];
//        [tree insertString:@"http://www.face.com"];
//        [tree insertString:@"http://blog.foo.com"];
//        [tree insertString:@"http://blog.foo.com/bar"];
//        
//        [tree insertString:@"http://chinese.hello.com/你好"];     // Supports Unicode languages!
//        [tree insertString:@"http://chinese.hello.com/你好吗?"];
//        
//        [tree saveTreeToFile:savePath];
//    }
//    
//    NSArray * retrieved = nil;
//    
//    // countLimit = 0 : no limit
//    // or use [tree retrievePrefix:@"http://"]
//    retrieved = [tree retrievePrefix:@"http://" countLimit:0];
//    NSLog(@"Return all matches: %@",retrieved);
//    
//    
//    // Return 2 items
//    retrieved = [tree retrievePrefix:@"http://" countLimit:2];
//    NSLog(@"Return 2 items: %@",retrieved);
//    
//    
//    // PJTernarySearchTree will cache the previous query to speed up retrieving (this is great for autocompletion) -- pruning
//    retrieved = [tree retrievePrefix:@"http://www." countLimit:0];
//    NSLog(@"Cached: %@",retrieved);
//    
//    
//    // Async
//    [tree retrievePrefix:@"http" countLimit:0 callback:^(NSArray *retrieved) {
//        NSLog(@"Callback: %@",retrieved);
//    }];
//    
//    
//    // Remove a string (or object)
//    // For non-string items, use [tree removeItem:obj];
//    [tree removeString:@"http://www.face.com"];
//    
//    retrieved = [tree retrievePrefix:@"http://www.fa" countLimit:0];
//    NSLog(@"Remove one: %@",retrieved);
//    
//    
//    // Test 2
//    
//    PJTernarySearchTree * tagSearchTree = [[PJTernarySearchTree alloc] init];
//    [tagSearchTree insertString:@"abc"];
//    
//    // Return nothing
//    NSLog(@"%@",[tagSearchTree retrievePrefix:@"abcd"]);
//    
//    // Nothing will happen...
//    [tagSearchTree insertString:@""];
//    [tagSearchTree insertString:nil];
//    
//    // retrieve 'nil' or @"" will get all items
//    [tagSearchTree insertString:@"def"];
//    [tagSearchTree insertString:@"ghi"];
//    NSLog(@"%@",[tagSearchTree retrievePrefix:nil]);
//    NSLog(@"%@",[tagSearchTree retrievePrefix:@""]);
//    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
