//
//  ViewController.m
//  AutoComplete
//
//  Created by Seth Hartwick on 5/5/15.
//  Copyright (c) 2015 Attainment Company. All rights reserved.
//

#import "ViewController.h"
#import "AttainmentAutoCompleteDictionary.h"

@interface ViewController() <UITableViewDataSource, UITableViewDelegate>
{
    AttainmentAutoCompleteDictionary* d;
    NSArray* suggestionData;
}
@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
  
    d = [[AttainmentAutoCompleteDictionary alloc] init];
    
//        NSArray* words = [d getSimilarWords:@"ly" limit:5];
//        NSLog(@"Words found");
//        NSArray* phrases = [d getMatchingGrams:@"about" limit:5];
//        NSLog(@"Phrases found");
//        pickerData = phrases;
//        self.picker.dataSource = self;
//        self.picker.delegate = self;
    
    [self.searchTextField addTarget:self action:@selector(editChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTextField becomeFirstResponder];
}

- (IBAction)editChanged:(id)sender {
    NSString* searchStr = self.searchTextField.text;
    
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    
    [d cancelPendingPredictions];
    
    [d predictionsForPhrase:searchStr limit:5 onComplete:^(NSString *phrase, NSArray *predictions) {
        if (![phrase isEqualToString:self.searchTextField.text]) return; // we already have a newer prediction pending
        
        suggestionData = predictions;
        
        CFAbsoluteTime timeTaken = CFAbsoluteTimeGetCurrent() - startTime;
        
        self.timeLabel.text = [NSString stringWithFormat:@"%lums", (unsigned long)(timeTaken * 1000)];
        
        [self.tableView reloadData];
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [suggestionData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [suggestionData count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *blah = [suggestionData objectAtIndex:indexPath.row];
    [cell.textLabel setText:blah];
    
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
