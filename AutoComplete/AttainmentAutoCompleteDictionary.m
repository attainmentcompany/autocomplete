//
//  Dictionary.m
//  
//
//  Created by Seth Hartwick on 5/5/15.
//
//

#import <Foundation/Foundation.h>
#import "AttainmentAutoCompleteDictionary.h"
#import "PJTernarySearchTree.h"

NSString * const DefaultDictionaryName = @"AutoCompleteDictionary.txt";
NSString * const PrePopulateBundleFileName = @"Dictionary";

@interface AttainmentAutoCompleteDictionary ()

@property NSString* content;

@property NSArray* n1gram;
@property NSArray* n2gram;
@property NSArray* n3gram;
@property NSArray* n4gram;
@property NSArray* n5gram;

@property NSArray* allGrams;


@property (nonatomic, strong) NSOperationQueue *predictionQueue;
@property (nonatomic, strong) NSOperationQueue *upkeepQueue;

@end


static inline void RunOnMainThread(void (^block)(void)) {
    if (NSThread.isMainThread) {
        block();
    }
    else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}


@implementation AttainmentAutoCompleteDictionary

#pragma mark Init and Setup

- (id)init {
    NSString *defaultDictionaryPath = [[AttainmentAutoCompleteDictionary libraryPath] stringByAppendingPathComponent:DefaultDictionaryName];
    
    NSString *prepopulatePath = [[NSBundle mainBundle] pathForResource:PrePopulateBundleFileName ofType:@"txt"];
    
    return [self initWithDictionary:defaultDictionaryPath prepopulatePath:prepopulatePath];
}

- (id)initWithDictionary:(NSString*)dictionaryPath prepopulatePath:(NSString*)prepopulatePath {
    self = [super init];
    
    if (self) {
        self.dictionaryPath = dictionaryPath;
        
        self.dictionaryPhraseLimit = 0;
        
        self.upkeepQueue = [[NSOperationQueue alloc] init];
        self.upkeepQueue.maxConcurrentOperationCount = 1;
        
        self.predictionQueue = [[NSOperationQueue alloc] init];
        self.predictionQueue.maxConcurrentOperationCount = 1;
        
        // QoS is only available starting with iOS 8
        if ([self.predictionQueue respondsToSelector:@selector(setQualityOfService:)]) {
            self.predictionQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        }
        
        [self openDictionaryWithPrePopulatePath:prepopulatePath];
    }
    
    return self;
}

- (void)openDictionaryWithPrePopulatePath:(NSString*)prepopulatePath {
    if (self.content) //already loaded
        return;
    
    NSStringEncoding encoding;
    
//    if (DEBUG){
//        NSLog(@"path to Dictionary file is:%@", self.dictionaryPath);
//    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.dictionaryPath]) {
        // dictionary already exists
        self.content = [NSString stringWithContentsOfFile:self.dictionaryPath usedEncoding:&encoding error:NULL];
        
        if (![self.content hasSuffix:@"\n"]) {
            self.content = [self.content stringByAppendingString:@"\n"];
        }
    }
    else if (prepopulatePath) {
        // copy to the library to allow additions
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError* error;
        
        [fileManager copyItemAtPath:prepopulatePath toPath:self.dictionaryPath  error:&error];
        
        self.content = [NSString stringWithContentsOfFile:self.dictionaryPath usedEncoding:&encoding error:NULL];\
        
        if (![self.content hasSuffix:@"\n"]) {
            self.content = [self.content stringByAppendingString:@"\n"];
        }
    }
    else {
        self.content = @"";
    }
    
    [self backgroundPrimeCache];
}

#pragma mark - Add to and Update Content

- (void)addToDictionary:(NSString *)phraseToAdd {
    [self.upkeepQueue addOperationWithBlock:^{
        [self immediatelyAddToDictionary:phraseToAdd];
    }];
}

- (void)immediatelyAddToDictionary:(NSString *)phraseToAdd {
    NSString *phrase = phraseToAdd;
    
    if (![phrase hasSuffix:@"\n"]) {
        phrase = [phrase stringByAppendingString:@"\n"];
    }
    
    NSString *newContent = [self.content stringByAppendingString:phrase];;
    
    NSArray *contentPhrases = [newContent componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    NSInteger lines = [contentPhrases count];
    NSUInteger limit = self.dictionaryPhraseLimit;
    
    if (lines > limit && limit != 0) {
        NSArray *reversedArray = [[contentPhrases reverseObjectEnumerator] allObjects];
        
        NSArray *limitedArray = [reversedArray subarrayWithRange:NSMakeRange(0, limit)];
        
        NSArray *newContentPhrases = [[limitedArray reverseObjectEnumerator] allObjects];
        
        newContent = [newContentPhrases componentsJoinedByString:@"\n"];
    }
    
    if (!self.preventSaving) {
        [newContent writeToFile:self.dictionaryPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    [self updateContent:newContent];
}

- (void)updateContent:(NSString*)newContent {
    NSArray *n2gram, *n3gram, *n4gram, *n5gram, *allGrams;
    
    n2gram = [self createNgrams:2 forContent:newContent];
    n3gram = [self createNgrams:3 forContent:newContent];
    n4gram = [self createNgrams:4 forContent:newContent];
    n5gram = [self createNgrams:5 forContent:newContent];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    [ma addObjectsFromArray:n2gram];
    [ma addObjectsFromArray:n3gram];
    [ma addObjectsFromArray:n4gram];
    [ma addObjectsFromArray:n5gram];
    
    allGrams = [ma copy];
    
    RunOnMainThread(^{
        if (n2gram) {
            self.n2gram = n2gram;
        }
        
        if (n3gram) {
            self.n3gram = n3gram;
        }
        
        if (n4gram) {
            self.n4gram = n4gram;
        }
        
        if (n5gram) {
            self.n5gram = n5gram;
        }
        
        if (allGrams) {
            self.allGrams = allGrams;
        }
        
        self.content = newContent;
    });
}

#pragma mark - Predicting

- (NSArray*)predictionsForPhrase:(NSString*)phrase {
    [self ensureGramsLoaded];
    
    return [AttainmentAutoCompleteDictionary predictionsForPhrase:phrase grams:self.allGrams];
}

- (NSArray*)predictionsForPhrase:(NSString*)phrase limit:(NSUInteger)n {
    NSArray* results = [self predictionsForPhrase:phrase];
    
    NSUInteger trueLimit = n;
    
    if ([results count] < n){
        trueLimit = [results count];
    }
    
    return [results subarrayWithRange:NSMakeRange(0,trueLimit)];
}

- (void)predictionsForPhrase:(NSString*)phrase onComplete:(PredictionCompleteBlock)onComplete {
    NSBlockOperation *predictionOperation = [NSBlockOperation blockOperationWithBlock:^{
        [self ensureGramsLoaded];

        NSArray *predictions = [self predictionsForPhrase:phrase];
        
        if (!predictionOperation.cancelled) {
            onComplete(phrase, predictions);
        }
    }];
    
    [self.predictionQueue addOperation:predictionOperation];
}

- (void)predictionsForPhrase:(NSString*)phrase limit:(NSUInteger)limit onComplete:(PredictionCompleteBlock)onComplete {
    NSBlockOperation *predictionOperation = [NSBlockOperation blockOperationWithBlock:^{
        [self ensureGramsLoaded];
        
        NSArray *predictions = [self predictionsForPhrase:phrase limit:limit];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (!predictionOperation.cancelled) {
                onComplete(phrase, predictions);
            }
        }];
    }];
    
    [self.predictionQueue addOperation:predictionOperation];
}

- (void)cancelPendingPredictions {
    [self.predictionQueue cancelAllOperations];
}

//- (NSArray*)getSimilarWords:(NSString*)word limit:(NSUInteger)count {
//    PJTernarySearchTree * tree = [[PJTernarySearchTree alloc]init];
//    NSArray * retrieved = nil;
//    NSMutableArray* ma = [self getNgrams:1];
//    NSArray* weightedWords = [self getWeightedArray:ma];
//    for( NSString* str in weightedWords) {
//        [tree insertString:str.lowercaseString];
//    }
//  //  NSLog(@"Start lookup");
//    retrieved = [tree retrievePrefix:[NSString stringWithFormat:@"%@", word.lowercaseString] countLimit:count];
// //   NSLog(@"%@", retrieved);
//
//    return retrieved;
//}

#pragma mark - Caching

- (void)ensureGramsLoaded {
    if (!self.allGrams) {
        [self primeCacheImmediately];
    }
}

- (void)backgroundPrimeCache {
    [self.upkeepQueue addOperationWithBlock:^{
        [self primeCacheImmediately];
    }];
}

- (void)primeCacheImmediately {
    [self loadGramsFromCache];
    
    if (!self.allGrams) {
        [self updateContent:self.content];
    }
}

- (NSString*)pathForNGramCache:(NSUInteger)n {
    NSString* filename = [[self.dictionaryPath lastPathComponent] stringByAppendingString:@"-"];
    switch(n) {
        case 1:
            filename = [filename stringByAppendingString:@"1gram.txt"];
            break;
        case 2:
            filename = [filename stringByAppendingString:@"2gram.txt"];
            break;
        case 3:
            filename = [filename stringByAppendingString:@"3gram.txt"];
            break;
        case 4:
            filename = [filename stringByAppendingString:@"4gram.txt"];
            break;
        default:
            //Anything 5 or great uses the same file
            filename = [filename stringByAppendingString:@"5gram.txt"];
            break;
    }
    
    return [[AttainmentAutoCompleteDictionary cachesPath] stringByAppendingPathComponent:filename];
}

- (NSArray*)createNgrams:(NSUInteger)n forContent:(NSString*)content {
    //Don't bother with 1 gram or less
    // if( n <= 1) return 0;
    
    NSArray *nGrams = [[self class] getNgrams:n forContent:content];
    
//    if (DEBUG) { NSLog(@" %lu grams returned", (unsigned long)n); }
    //  NSArray* wa =  [self getWeightedArray:ma];
    //  NSLog(@"%@", wa);
    
    
    if (!self.preventSaving) {
        NSString *grams = [nGrams componentsJoinedByString:@"\n"];
        
        NSString *fullpath = [self pathForNGramCache:n];
        
        NSError *error;
        
        BOOL ok = [grams writeToFile:fullpath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        if (!ok){
            NSLog(@"Error writing file at %@\n%@", fullpath, error);
        }
    }
    
    return [nGrams copy];
}

- (void)loadGramsFromCache {
    NSString* path;
    NSStringEncoding encoding;
    NSString* tempGrams;
    NSString *phraseSep = @"\n";
    NSCharacterSet* phraseSet = [NSCharacterSet characterSetWithCharactersInString:phraseSep];
    
    NSArray *n2gram, *n3gram, *n4gram, *n5gram, * allGrams;
    
    {
        path = [self pathForNGramCache:2];
        tempGrams = [NSString stringWithContentsOfFile:path usedEncoding:&encoding error:NULL];
        n2gram  = [tempGrams componentsSeparatedByCharactersInSet:phraseSet];
        
//        if(DEBUG) NSLog(@"Loading 2 grams");
    }
    
    {
        path = [self pathForNGramCache:3];
        tempGrams = [NSString stringWithContentsOfFile:path usedEncoding:&encoding error:NULL];
        n3gram  = [tempGrams componentsSeparatedByCharactersInSet:phraseSet];
        
//        if(DEBUG) NSLog(@"Loading 3 grams");
    }
    
    {
        path = [self pathForNGramCache:4];
        tempGrams = [NSString stringWithContentsOfFile:path usedEncoding:&encoding error:NULL];
        n4gram  = [tempGrams componentsSeparatedByCharactersInSet:phraseSet];
        
//        if(DEBUG) NSLog(@"Loading 4 grams");
    }
    
    {
        path = [self pathForNGramCache:4];
        tempGrams = [NSString stringWithContentsOfFile:path usedEncoding:&encoding error:NULL];
        n5gram  = [tempGrams componentsSeparatedByCharactersInSet:phraseSet];
        
//        if(DEBUG) NSLog(@"Loading 5 grams");
    }
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    [ma addObjectsFromArray:n2gram];
    [ma addObjectsFromArray:n3gram];
    [ma addObjectsFromArray:n4gram];
    [ma addObjectsFromArray:n5gram];
    
    allGrams = [ma copy];
    
    RunOnMainThread(^{
        if (n2gram) {
            self.n2gram = n2gram;
        }
        
        if (n3gram) {
            self.n3gram = n3gram;
        }
        
        if (n4gram) {
            self.n4gram = n4gram;
        }
        
        if (n5gram) {
            self.n5gram = n5gram;
        }
        
        if (allGrams) {
            self.allGrams = allGrams;
        }
    });
}

+ (NSArray*)predictionsForPhrase:(NSString*)phrase grams:(NSArray*)grams {
    phrase = [self sanitizeString:phrase];
    
    NSPredicate* pred = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", phrase];
    NSArray * tempArray = [grams filteredArrayUsingPredicate:pred];
    
    //if (DEBUG) NSLog(@"Start weighting");
    
    NSArray* wa = [AttainmentAutoCompleteDictionary getWeightedArray:tempArray];
    
    //if (DEBUG) NSLog(@"Finished weighting");
    
    return wa;
}


+ (NSMutableArray*)getNgrams:(NSUInteger)n forContent:(NSString*)content {
    NSMutableArray* ngrams = [NSMutableArray array];
    NSString *phraseSep = @"\n";
    NSCharacterSet* phraseSet = [NSCharacterSet characterSetWithCharactersInString:phraseSep];
    NSString *wordSep = @" ";
    NSCharacterSet* wordSet = [NSCharacterSet characterSetWithCharactersInString:wordSep];
    
    NSArray* phrases = [content componentsSeparatedByCharactersInSet:phraseSet];
    for (NSUInteger j = 0; j < phrases.count; j++) {
        NSString* phrase = [phrases objectAtIndex:j];
        NSArray* words = [phrase componentsSeparatedByCharactersInSet:wordSet];
        if (words.count >= n)  {
            for (NSUInteger i = 0; i < words.count - n+1; i++) {
                [ngrams addObject: [NSString stringWithFormat:@"%@", [self concat:words start:i end:i+n]]];
            }
        }
    }
    return ngrams;
}

+ (NSArray*)getWeightedArray:(NSArray*)phrases {
    NSCountedSet *set = [[NSCountedSet alloc] initWithArray:phrases];
    NSMutableArray* dictArray = [NSMutableArray array];
    [set enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
        [dictArray addObject:@{@"object": obj,
                               @"count":@([set countForObject:obj])}];
    }];
    NSArray* sa = [set.allObjects sortedArrayUsingComparator:^(id obj1, id obj2) {
        NSUInteger n = [set countForObject:obj1];
        NSUInteger m = [set countForObject:obj2];
        return (n <= m)? (n < m)?  NSOrderedDescending : NSOrderedSame : NSOrderedAscending;
    }];
    //  NSArray* sa =[dictArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO]]];
    
    return sa;
}

+ (NSString*)concat:(NSArray*)words start:(NSUInteger)start end:(NSUInteger)end {
    NSMutableString* ms = [[NSMutableString alloc]init];
    
    for (NSUInteger i = start; i < end;i++) {
        NSString* str;
        
        if (i > start) {
            str = [NSString stringWithFormat:@" %@", words[i]];
        }
        else {
            str = [NSString stringWithFormat:@"%@", words[i]];
        }
        
        [ms appendString:str];
    }
    
    return [self sanitizeString:[NSString stringWithString:ms]];
}

+ (NSString *)sanitizeString:(NSString*)str {
    
    NSString *sanitizedString = [str             stringByReplacingOccurrencesOfString:@"(" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@")" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"?" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"'" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    sanitizedString           = [sanitizedString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return sanitizedString;
}

+ (NSString*)libraryPath {
    static dispatch_once_t pred;
    static NSString *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        [[NSFileManager defaultManager] createDirectoryAtPath:shared withIntermediateDirectories:YES attributes:nil error:nil];
    });

    return shared;
}

+ (NSString*)cachesPath {
    static dispatch_once_t pred;
    static NSString *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        [[NSFileManager defaultManager] createDirectoryAtPath:shared withIntermediateDirectories:YES attributes:nil error:nil];
    });
    
    return shared;
}

@end