//
//  Dictionary.h
//  AutoComplete
//
//  Created by Seth Hartwick on 5/5/15.
//  Copyright (c) 2015 Attainment Company. All rights reserved.
//

#ifndef AutoComplete_Dictionary_h
#define AutoComplete_Dictionary_h

typedef void (^PredictionCompleteBlock)(NSString *phrase, NSArray *predictions);


@interface AttainmentAutoCompleteDictionary: NSObject

@property (nonatomic, strong) NSString *dictionaryPath;

@property (nonatomic, assign) BOOL preventSaving;

@property (nonatomic, assign) NSUInteger dictionaryPhraseLimit;

- (id)initWithDictionary:(NSString*)dictionaryPath prepopulatePath:(NSString*)prepopulatePath;

- (void)addToDictionary:(NSString *)phrase;


- (NSArray*)predictionsForPhrase:(NSString*)phrase;
- (NSArray*)predictionsForPhrase:(NSString*)phrase limit:(NSUInteger)n;

- (void)predictionsForPhrase:(NSString*)phrase onComplete:(PredictionCompleteBlock)onComplete;
- (void)predictionsForPhrase:(NSString*)phrase limit:(NSUInteger)n onComplete:(PredictionCompleteBlock)onComplete;
- (void)cancelPendingPredictions;


//-(NSArray*)getSimilarWords:(NSString*)word limit:(NSUInteger)count;




@end
#endif
